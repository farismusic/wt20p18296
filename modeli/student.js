const Sequelize = require("sequelize");

module.exports = function (sequelize, DataTypes) {
    const Student = sequelize.define("student", {
        ime: Sequelize.STRING,
        indeks: {
            type: Sequelize.STRING,
            unique: true

        }
    })
    return Student;
};