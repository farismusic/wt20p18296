const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Tip = sequelize.define("tip",{
        naziv: {
            type : Sequelize.STRING,
            unique : true
        }
    })
    return Tip;
};