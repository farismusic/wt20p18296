const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Dan = sequelize.define("dan",{
        naziv: {
            type : Sequelize.STRING,
            unique : true
        }
    })
    return Dan;
};