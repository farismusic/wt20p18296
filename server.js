const express = require("express");
const fs = require("fs");
const bodyParser = require("body-parser");
const app = express();
const baza = require("./baza");
const { resolve } = require("path");
const { rejects } = require("assert");
const { student } = require("./baza");

app.use(bodyParser.json());
app.use(express.static('public'));

//Validacija kada smo radili za .txt datotekama
function validirajAktivnost(req, akt) {
    var nova = { naziv: req.body["naziv"], tip: req.body["tip"], pocetak: parseFloat(req.body["pocetak"]), kraj: parseFloat(req.body["kraj"]), dan: req.body["dan"] };

    if (nova.pocetak >= nova.kraj || (nova.pocetak < 0 || nova.pocetak > 24) || (nova.kraj < 0 || nova.kraj > 24)) {
        return 0;
    }

    if (!(nova.pocetak >= 8 && nova.pocetak < 20) || !(nova.kraj > 8 && nova.kraj <= 20)) {
        return 0;
    }

    var regex = /^[0-9]{1,2}(\.5)?$/;

    if (nova.pocetak.toString().search(regex) == -1 || nova.kraj.toString().search(regex) == -1) {
        return 0;
    }

    var niz = [];
    for (let i = 0; i < akt.length; i++) {

        niz = akt[i].split(",");
        var stara = { naziv: niz[0], tip: niz[1], pocetak: parseFloat(niz[2]), kraj: parseFloat(niz[3]), dan: niz[4] };

        if (nova.dan.toLowerCase() == stara.dan.toLowerCase()) {

            if (!((nova.pocetak < stara.pocetak && nova.kraj <= stara.pocetak) || (nova.pocetak >= stara.kraj && nova.kraj > stara.kraj))) {
                return 0;
            }
        }

    }

    return 1;

}

function validirajAktivnostDB(req, akt) {
    let nova = { naziv: req.body["naziv"].trim(), pocetak: parseFloat(req.body["pocetak"]), kraj: parseFloat(req.body["kraj"]), dan: req.body["danId"] };

    if (nova.pocetak >= nova.kraj || (nova.pocetak < 0 || nova.pocetak > 24) || (nova.kraj < 0 || nova.kraj > 24)) {
        return false;
    }

    if (!(nova.pocetak >= 8 && nova.pocetak < 20) || !(nova.kraj > 8 && nova.kraj <= 20)) {
        return false;
    }

    var regex = /^[0-9]{1,2}(\.5)?$/;

    if (nova.pocetak.toString().search(regex) == -1 || nova.kraj.toString().search(regex) == -1) {
        return false;
    }

    for (let i = 0; i < akt.length; i++) {

        var stara = { naziv: akt[i].naziv, pocetak: akt[i].pocetak, kraj: akt[i].kraj, dan: akt[i].danId };

        if (nova.dan == stara.dan) {

            if (!((nova.pocetak < stara.pocetak && nova.kraj <= stara.pocetak) || (nova.pocetak >= stara.kraj && nova.kraj > stara.kraj))) {
                return false;
            }
        }

    }

    return true;

}

function inicijaliziraj() {
    return new Promise((resolve, reject) => {

        baza.predmet.create({ naziv: "Programski jezici i prevodioci" }).then((p) => {

            var nizPromisea = [];

            nizPromisea.push(baza.dan.create({ naziv: "Petak" }));

            nizPromisea.push(baza.tip.create({ naziv: "Predavanje" }));

            nizPromisea.push(baza.grupa.create({ naziv: "PJP grupa 3", predmetId: p.id }).then((g) => {

                nizPromisea.push(baza.student.create({ ime: "Faris Mušić", indeks: "18296" }).then((s) => {
                    s.setGrupe([g]);
                    return new Promise((resolve, reject) => { resolve(s); });
                }));

                return new Promise((resolve, reject) => { resolve(g); })

            }));

            Promise.all(nizPromisea).then((rez) => {
                baza.aktivnost.create({ naziv: p.naziv + " " + rez[1].dataValues.naziv, pocetak: 9, kraj: 12, predmetId: p.id, danId: rez[0].dataValues.id, tipId: rez[1].dataValues.id, grupaId: rez[2].dataValues.id }).then((a) => {
                    resolve(a);
                });
            });

            resolve(p);
        });

    });
}

app.get("/", function (req, res) {
    res.set("Content-Type", "text/html");
    res.sendFile(__dirname + "/public/spirala2rasporedi.html")
});

app.post("/v1/predmet", function (req, res) {


    var noviPredmet = req.body["naziv"].trim();
    var postojiLi = false;
    var odgovor;
    var predmeti;

    fs.readFile("predmeti.txt", function (err, data) {
        if (err) {
            throw err;
        } else {
            predmeti = data.toString().split("\n");
            predmeti.pop();

            for (let i = 0; i < predmeti.length; i++) {
                if (predmeti[i].toLowerCase() == noviPredmet.toLowerCase()) {
                    postojiLi = true;
                }
            }

            if (!postojiLi) {
                odgovor = { message: "Uspješno dodan predmet!" }
                fs.appendFile('predmeti.txt', noviPredmet + "\n", function (err) {
                    if (err) throw err;
                });
            } else {
                odgovor = { message: "Naziv predmeta postoji!" };
            }

            res.json(odgovor);

        }
    });

});

app.post("/v1/aktivnost", function (req, res) {

    var novaAktivnost = req.body["naziv"].trim() + "," + req.body["tip"].trim() + "," + req.body["pocetak"].toString().trim() + "," + req.body["kraj"].toString().trim() + "," + req.body["dan"].trim();
    var aktivnosti = [];

    fs.readFile("aktivnosti.txt", function (err, data) {
        if (err) throw err;
        aktivnosti = data.toString().split("\n");
        aktivnosti.pop();

        var odgovor;
        /*Validiraj novu aktivnost sa postojećim*/
        if (validirajAktivnost(req, aktivnosti) == 1) {

            odgovor = { message: "Uspješno dodana aktivnost!" };
            fs.appendFile('aktivnosti.txt', novaAktivnost + "\n", function (err) {
                if (err) throw err;
            });

        } else {
            odgovor = { message: "Aktivnost nije validna!" };
        }
        res.json(odgovor);

    });

});

app.get("/v1/aktivnosti", function (req, res) {

    var aktivnosti = [];

    fs.readFile("aktivnosti.txt", function (err, data) {
        if (err) throw err;
        aktivnosti = data.toString().split("\n");
        aktivnosti.pop();

        let objekti = [];
        let red;
        for (let i = 0; i < aktivnosti.length; i++) {
            red = aktivnosti[i].split(",");
            objekti.push({ naziv: red[0], tip: red[1], pocetak: parseFloat(red[2]), kraj: parseFloat(red[3]), dan: red[4] });
        }
        res.json(objekti);

    });

});

app.get("/v1/predmeti", function (req, res) {

    var predmeti;

    fs.readFile("predmeti.txt", function (err, data) {
        if (err) {
            throw err;
        } else {
            predmeti = data.toString().split("\n");

            predmeti.pop();

            let objekti = [];
            for (let i = 0; i < predmeti.length; i++) {
                objekti.push({ naziv: predmeti[i] });
            }
            res.json(objekti);
        }
    });
})

app.delete("/v1/all", function (req, res) {

    var odgovor;

    fs.writeFile("aktivnosti.txt", "", function (err) {
        if (err) {
            odgovor = { message: "Greška - sadržaj datoteka nije moguće obrisati!" };
            res.json(odgovor);
            throw err;
        } else {
            odgovor = { message: "Uspješno obrisan sadržaj datoteka!" };
        }
    });

    fs.writeFile("predmeti.txt", "", function (err) {
        if (err) {
            odgovor = { message: "Greška - sadržaj datoteka nije moguće obrisati!" };
            res.json(odgovor);
            throw err;
        } else {
            odgovor = { message: "Uspješno obrisan sadržaj datoteka!" };
            res.json(odgovor);
        }
    });



});

app.get("/v1/predmet/:naziv/aktivnost", function (req, res) {

    var aktivnosti = [];
    var parametar = req.params["naziv"].trim();

    fs.readFile("aktivnosti.txt", function (err, data) {
        if (err) throw err;
        aktivnosti = data.toString().split("\n");
        aktivnosti.pop();

        let objekti = [];
        let red;
        for (let i = 0; i < aktivnosti.length; i++) {
            red = aktivnosti[i].split(",");
            if (red[0].toLowerCase() == parametar.toLowerCase()) {
                objekti.push({ naziv: red[0], tip: red[1], pocetak: red[2], kraj: red[3], dan: red[4] });
            }
        }
        res.json(objekti);

    });

});

app.delete("/v1/aktivnost/:naziv", function (req, res) {

    var parametar = req.params["naziv"].trim();

    var odgovor;

    fs.readFile("aktivnosti.txt", function (err, data) {
        if (err) throw err;
        aktivnosti = data.toString().split("\n");
        aktivnosti.pop();

        brojAktivnosti = aktivnosti.length;
        var red;

        for (let i = 0; i < aktivnosti.length; i++) {

            red = aktivnosti[i].split(",");

            if (red[0].toLowerCase() == parametar.toLowerCase()) {
                odgovor = { message: "Uspješno obrisana aktivnost!" };
                aktivnosti.splice(i--, 1);
            }

        }

        if (brojAktivnosti == aktivnosti.length) {
            odgovor = { message: "Greška - aktivnost nije obrisana!" };
        }

        var aktivnostiString = "";

        for (let i = 0; i < aktivnosti.length; i++) {
            aktivnostiString = aktivnostiString + aktivnosti[i] + "\n";
        }

        fs.writeFile("aktivnosti.txt", aktivnostiString, function (err) {
            if (err) {
                odgovor = { message: "Greška - aktivnost nije obrisana!" };
                res.json(odgovor);
                throw err;
            } else {
                res.json(odgovor);
            }
        });

    });

});

app.delete("/v1/predmet/:naziv", function (req, res) {

    var parametar = req.params["naziv"].trim();

    var odgovor;

    fs.readFile("predmeti.txt", function (err, data) {
        if (err) throw err;
        predmeti = data.toString().split("\n");
        predmeti.pop();

        brojPredmeta = predmeti.length;
        var red;

        for (let i = 0; i < predmeti.length; i++) {

            if (predmeti[i].toLowerCase() == parametar.toLowerCase()) {
                odgovor = { message: "Uspješno obrisan predmet!" };
                predmeti.splice(i--, 1);
            }

        }

        if (brojPredmeta == predmeti.length) {
            odgovor = { message: "Greška - predmet nije obrisan!" };
        }

        var predmetiString = "";

        for (let i = 0; i < predmeti.length; i++) {
            predmetiString = predmetiString + predmeti[i] + "\n";
        }

        fs.writeFile("predmeti.txt", predmetiString, function (err) {
            if (err) {
                odgovor = { message: "Greška - predmet nije obrisan!" };
                res.json(odgovor);
                throw err;
            } else {
                res.json(odgovor);
            }
        });

    });

});

baza.sequelize.sync({ force: true }).then(function () {
    inicijaliziraj().then(() => {
        console.log("Baza inicijalizirana");
    }).catch(() => {
        console.log("Greška pri inicijalizaciji baze");
    });
}).catch(() => {
    console.log("Greška pri inicijalizaciji baze");
});

// --------------- Rad sa bazom ---------------
app.post("/v2/addStudents", (req, res) => {

    let studenti = JSON.parse(JSON.stringify(req.body));

    if (studenti.length < 2) {
        res.status(200).json({ message: "Podaci nisu validni!" });
        return;
    } else {

    }

    var odgovori = [];
    let promises = []

    promises.push(baza.grupa.findById(parseInt(studenti[studenti.length - 1]["id"])).then((grupa) => {

        if (grupa == null) {
            res.status(200).json({ message: "Nepostojeća grupa" });
            return;
        }

        for (let i = 0; i < studenti.length; i++) {

            if (studenti[i].ime == null || studenti[i].indeks == null) {
                continue;
            }

            promises.push(baza.student.findAndCountAll({ where: { ime: studenti[i].ime, indeks: studenti[i].indeks } }).then((rez) => {

                if (rez.count == 0) {
                    //Ako student ne postoji dodaj ga

                    return baza.student.create({ ime: studenti[i].ime, indeks: studenti[i].indeks }).then((addedStud) => {

                        //Dodaj studenta u grupu
                        return addedStud.setGrupe([grupa]).then(() => {
                            return new Promise((resolve, reject) => {
                                resolve(addedStud);
                            });
                        }).catch(() => { res.status(200).json({ message: "Greška !" }); return; });


                    }).catch(() => {

                        //Ako indeks u bazi već postoji
                        return baza.student.findOne({ where: { indeks: studenti[i].indeks } }).then((errStud) => {

                            return new Promise((resolve, reject) => {
                                odgovori.push({ message: "Student " + studenti[i].ime + " nije kreiran jer postoji student " + errStud.ime + " sa istim indeksom " + errStud.indeks });
                                //console.log("Student " + studenti[i].ime + " nije kreiran jer postoji student " + errStud.ime + " sa istim indeksom " + errStud.indeks);
                                resolve(errStud);
                            });

                        });

                    })


                } else {

                    //Ako mijenjamo grupu studenta na nekom predmetu
                    promises.push(
                        rez.rows[0].getGrupe().then((grupeStudenta) => {

                            let novaGrupa = true;

                            for (let i = 0; i < grupeStudenta.length; i++) {
                                if (grupeStudenta[i].predmetId == grupa.predmetId) {

                                    grupeStudenta[grupeStudenta.indexOf(grupeStudenta[i])] = grupa;
                                    novaGrupa = false;
                                    return rez.rows[0].setGrupe(grupeStudenta).then(() => {
                                        return new Promise((resolve, reject) => {
                                            resolve(grupeStudenta);
                                        });
                                    }).catch(() => { res.status(200).json({ message: "Greška !" }); return; });


                                }
                            }

                            if (novaGrupa) {
                                grupeStudenta.push(grupa);
                                return rez.rows[0].setGrupe(grupeStudenta).then(() => {
                                    return new Promise((resolve, reject) => {
                                        resolve(grupeStudenta);
                                    });
                                }).catch(() => { res.status(200).json({ message: "Greška !" }); return; });
                            }

                        }).catch(() => { res.status(200).json({ message: "Greška !" }); return; }));
                }
            }).catch(() => { res.status(200).json({ message: "Greška !" }); return; }));

        }

        Promise.all(promises).then(() => {
            res.status(200).json(odgovori);
            return;
        }).catch(() => { res.status(200).json({ message: "Greška !" }); return; });


    }).catch(() => { res.status(200).json({ message: "Greška !" }); return; }));

    studenti.pop();

});

app.get("/v2/studenti", (req, res) => {
    baza.student.findAll().then((studenti) => {
        let objekti = [];
        studenti.forEach(el => {
            objekti.push({ ime: el.ime, indeks: el.indeks });
        });
        res.json(objekti);
    }).catch(() => {
        res.json({ message: "Greška pri dohvaćanju studenata" });
    });
});

app.get("/v2/student/:id", (req, res) => {
    baza.student.findOne({ where: { id: req.params["id"] } }).then((s) => {
        res.json({ ime: s.ime, indeks: s.indeks });
    }).catch(() => {
        res.json({ message: "Ne postoji student sa tim ID" });
    });
});

app.post("/v2/student", (req, res) => {
    baza.student.create({ ime: req.body["ime"].trim(), indeks: req.body["indeks"].trim() }).then((s) => {
        res.json({ message: "Student " + s.ime + " je uspješno dodan u bazu" });
    }).catch(() => {
        res.json({ message: "Dodavanje studenta nije uspjelo" });
    });
});

app.delete("/v2/student/:id", (req, res) => {
    baza.student.findOne({ where: { id: req.params["id"] } }).then((s) => {
        s.destroy();
        res.json({ message: "Student " + s.ime + " je obrisan!" });
    }).catch(() => { res.json({ message: "Greška pri prisanju studenta !" }) });
});

app.put("/v2/student/:id", (req, res) => {
    var novi = { ime: req.body["ime"].trim(), indeks: req.body["indeks"].trim() };
    baza.student.findOne({ where: { id: req.params["id"] } }).then((s) => {
        s.ime = novi.ime;
        s.indeks = novi.indeks;
        s.save();
        res.json({ message: "Student uspješno ažuriran" });
    }).catch(() => { res.json({ message: "Greška pri ažuriranje studenta !" }) });
});


app.get("/v2/predmeti", (req, res) => {

    baza.predmet.findAll().then((ps) => {
        let objekti = [];
        ps.forEach(el => {
            objekti.push({ id: el.id, naziv: el.naziv });
        });
        res.json(objekti);
    }).catch(() => {
        res.json({ message: "Greška pri dohvaćanju predmeta" });
    });
});

app.get("/v2/predmet/:id", (req, res) => {
    baza.predmet.findOne({ where: { id: req.params["id"] } }).then((p) => {
        res.json({ naziv: p.naziv });
    }).catch(() => {
        res.json({ message: "Ne postoji predmet sa tim ID" });
    });
});

app.post("/v2/predmet", (req, res) => {
    baza.predmet.create({ naziv: req.body["naziv"].trim() }).then((p) => {
        res.json({ message: "Predmet " + p.naziv + " je uspješno dodan u bazu" });
    }).catch(() => {
        res.json({ message: "Dodavanje predmeta nije uspjelo" });
    });
});

app.delete("/v2/predmet/:id", (req, res) => {
    baza.predmet.findOne({ where: { id: req.params["id"] } }).then((p) => {
        p.destroy();
        res.json({ message: "Predmet je obrisan" });
    }).catch(() => { res.json({ message: "Greška pri prisanju predmeta !" }) });
});

app.put("/v2/predmet/:id", (req, res) => {
    var novi = { naziv: req.body["naziv"].trim() };
    baza.predmet.findOne({ where: { id: req.params["id"] } }).then((p) => {
        p.naziv = novi.naziv;
        p.save();
        res.json({ message: "Predmet uspješno ažuriran" });
    }).catch(() => { res.json({ message: "Greška pri ažuriranje predmeta !" }) });
});


app.get("/v2/grupe", (req, res) => {

    baza.grupa.findAll().then((gs) => {
        let objekti = [];
        gs.forEach(el => {
            objekti.push({ id: el.id, naziv: el.naziv });
        });
        res.json(objekti);
    }).catch(() => {
        res.json({ message: "Greška pri dohvaćanju grupa" });
    });
});

app.get("/v2/grupa/:id", (req, res) => {
    baza.grupa.findOne({ where: { id: req.params["id"] } }).then((g) => {
        res.json({ naziv: g.naziv });
    }).catch(() => {
        res.json({ message: "Ne postoji grupa sa tim ID" });
    });
});

app.get("/v2/grupePredmeta/:id", (req, res) => {

    let predmet = req.params["id"];

    baza.predmet.findById(parseInt(predmet)).then((p) => {

        p.getGrupePredmeta().then((grupes) => {

            let objekti = [];
            grupes.forEach(g => {
                objekti.push({id : g.id, naziv : g.naziv});
            });
            res.json(objekti);

        }).catch(() => {
            res.status(200).json({message : "Neuspješno dohvatanje grupa predmeta"});
        });

    }).catch(() => {
        res.status(200).json({message : "Neuspješno dohvatanje grupa predmeta"});
    });

});

app.post("/v2/grupa", (req, res) => {
    baza.grupa.create({ naziv: req.body["naziv"].trim(), predmetId: req.body["predmetId"] }).then((g) => {
        res.json({ message: "Grupa " + g.naziv + " je uspješno dodana u bazu" });
    }).catch(() => {
        res.json({ message: "Dodavanje grupe nije uspjelo" });
    });
});

app.delete("/v2/grupa/:id", (req, res) => {
    baza.grupa.findOne({ where: { id: req.params["id"] } }).then((g) => {
        g.destroy();
        res.json({ message: "Grupa je obrisana" });
    }).catch(() => { res.json({ message: "Greška pri prisanju grupe !" }) });
});

app.put("/v2/grupa/:id", (req, res) => {
    var nova = { naziv: req.body["naziv"].trim(), predmetId: req.body["predmetId"] };
    baza.grupa.findOne({ where: { id: req.params["id"] } }).then((g) => {
        g.naziv = nova.naziv;
        g.predmetId = nova.predmetId;
        g.save();
        res.json({ message: "Grupa uspješno ažurirana" });
    }).catch(() => { res.json({ message: "Greška pri ažuriranje grupe !" }) });
});


app.get("/v2/tipovi", (req, res) => {

    baza.tip.findAll().then((ts) => {
        let objekti = [];
        ts.forEach(el => {
            objekti.push({ id: el.id, naziv: el.naziv });
        });
        res.json(objekti);
    }).catch(() => {
        res.json({ message: "Greška pri dohvaćanju tipova" });
    });
});

app.get("/v2/tip/:id", (req, res) => {
    baza.tip.findOne({ where: { id: req.params["id"] } }).then((t) => {
        res.json({ naziv: t.naziv });
    }).catch(() => {
        res.json({ message: "Ne postoji tip sa tim ID" });
    });
});

app.post("/v2/tip", (req, res) => {
    baza.tip.create({ naziv: req.body["naziv"].trim() }).then((t) => {
        res.json({ message: "Tip " + t.naziv + " je uspješno dodan u bazu" });
    }).catch(() => {
        res.json({ message: "Dodavanje tipa nije uspjelo" });
    });
});

app.delete("/v2/tip/:id", (req, res) => {
    baza.tip.findOne({ where: { id: req.params["id"] } }).then((t) => {
        t.destroy();
        res.json({ message: "Tip je obrisan" });
    }).catch(() => { res.json({ message: "Greška pri prisanju tipa !" }) });
});

app.put("/v2/tip/:id", (req, res) => {
    var novi = { naziv: req.body["naziv"].trim() };
    baza.tip.findOne({ where: { id: req.params["id"] } }).then((t) => {
        t.naziv = novi.naziv;
        t.save();
        res.json({ message: "Tip uspješno ažuriran" });
    }).catch(() => { res.json({ message: "Greška pri ažuriranje tipa !" }) });
});


app.get("/v2/dani", (req, res) => {

    baza.dan.findAll().then((ds) => {
        let objekti = [];
        ds.forEach(el => {
            objekti.push({ id: el.id, naziv: el.naziv });
        });
        res.json(objekti);
    }).catch(() => {
        res.json({ message: "Greška pri dohvaćanju dana" });
    });
});

app.get("/v2/dan/:id", (req, res) => {
    baza.dan.findOne({ where: { id: req.params["id"] } }).then((d) => {
        res.json({ naziv: d.naziv });
    }).catch(() => {
        res.json({ message: "Ne postoji dan sa tim ID" });
    });
});

app.post("/v2/dan", (req, res) => {
    baza.dan.create({ naziv: req.body["naziv"].trim() }).then((d) => {
        res.json({ message: "Dan " + d.naziv + " je uspješno dodan u bazu" });
    }).catch(() => {
        res.json({ message: "Dodavanje dana nije uspjelo" });
    });
});

app.delete("/v2/dan/:id", (req, res) => {
    baza.dan.findOne({ where: { id: req.params["id"] } }).then((d) => {
        d.destroy();
        res.json({ message: "Dan je obrisan" });
    }).catch(() => { res.json({ message: "Greška pri prisanju dana !" }) });
});

app.put("/v2/dan/:id", (req, res) => {
    var novi = { naziv: req.body["naziv"].trim() };
    baza.dan.findOne({ where: { id: req.params["id"] } }).then((d) => {
        d.naziv = novi.naziv;
        d.save();
        res.json({ message: "Dan uspješno ažuriran" });
    }).catch(() => { res.json({ message: "Greška pri ažuriranje dana !" }) });
});


app.get("/v2/aktivnosti", (req, res) => {

    baza.aktivnost.findAll().then((as) => {
        let objekti = [];
        as.forEach(el => {
            objekti.push({ naziv: el.naziv, pocetak: el.pocetak, kraj: el.kraj });
        });
        res.json(objekti);
    }).catch(() => {
        res.json({ message: "Greška pri dohvaćanju aktivnosti" });
    });
});

app.get("/v2/aktivnost/:id", (req, res) => {
    baza.aktivnost.findOne({ where: { id: req.params["id"] } }).then((a) => {
        res.json({ naziv: a.naziv, pocetak: a.pocetak, kraj: a.kraj });
    }).catch(() => {
        res.json({ message: "Ne postoji aktivnost sa tim ID" });
    });
});

app.post("/v2/aktivnost", (req, res) => {

    baza.aktivnost.findAll().then((as) => {

        if (validirajAktivnostDB(req, as)) {

            baza.aktivnost.create({
                naziv: req.body["naziv"].trim(), pocetak: parseFloat(req.body["pocetak"]), kraj: parseFloat(req.body["kraj"]), predmetId: req.body["predmetId"],
                danId: req.body["danId"], tipId: req.body["tipId"], grupaId: req.body["grupaId"]
            }).then((d) => {
                res.json({ message: "Uspješno dodana aktivnost!" });
            }).catch(() => {
                res.json({ message: "Dodavanje aktivnosti nije uspjelo" });
            });

        } else {
            res.json({ message: "Aktivnost nije validna" });
        }
    }).catch(() => {
        res.json({ message: "Greška pri dohvaćanju aktivnosti" });
    });
});

app.delete("/v2/aktivnost/:id", (req, res) => {
    baza.aktivnost.findOne({ where: { id: req.params["id"] } }).then((a) => {
        a.destroy();
        res.json({ message: "Aktivnost je obrisana" });
    }).catch(() => { res.json({ message: "Greška pri prisanju aktivnosti !" }) });
});

app.put("/v2/aktivnost/:id", (req, res) => {

    baza.aktivnost.findAll().then((as) => {

        if (validirajAktivnostDB(req, as)) {

            var nova = {
                naziv: req.body["naziv"].trim(), pocetak: parseFloat(req.body["pocetak"]), kraj: parseFloat(req.body["kraj"]), predmetId: req.body["predmetId"],
                danId: req.body["danId"], tipId: req.body["tipId"], grupaId: req.body["grupaId"]
            };
            baza.aktivnost.findOne({ where: { id: req.params["id"] } }).then((a) => {
                a.naziv = nova.naziv;
                a.pocetak = nova.pocetak;
                a.kraj = nova.kraj;
                a.predmetId = nova.predmetId;
                a.danId = nova.danId;
                a.tipId = nova.tipId;
                a.grupaId = nova.grupaId;
                a.save();
                res.json({ message: "Aktivnost uspješno ažurirana" });
            }).catch(() => { res.json({ message: "Greška pri ažuriranju aktivnosti !" }) });

        } else {
            res.json({ message: "Aktivnost nije validna" });
        }
    }).catch(() => {
        res.json({ message: "Greška pri dohvaćanju aktivnosti" });
    });

});
// --------------- Rad sa bazom ---------------


const port = 3000 || process.env.PORT;

app.listen(port);

module.exports = app;