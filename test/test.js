const chai = require("chai");
const server = require("../server");
const chaiHttp = require("chai-http");
const fs = require("fs");
const { expect } = require("chai");

chai.use(chaiHttp);
chai.should();
chai.expect();

var predmeti = [];
var aktivnosti = [];
var testovi = [];
var zahtjev;
var indeksi = [];
j = 0;

function ucitajPredmete() {
    try {
        predmeti = fs.readFileSync('predmeti.txt', 'utf8').toString().split("\n");
    } catch (e) {
        console.log('Error:', e.stack);
    }
}

function ucitajAktivnosti() {
    try {
        aktivnosti = fs.readFileSync('aktivnosti.txt', 'utf8').toString().split("\n");
    } catch (e) {
        console.log('Error:', e.stack);
    }
}

function ucitajTestove() {
    try {
        testovi = fs.readFileSync('testniPodaci.txt', 'utf8').toString().split("\n");
    } catch (e) {
        console.log('Error:', e.stack);
    }
}

ucitajTestove();


describe("Testovi", () => {


    for (let i = 0; i < testovi.length; i++) {

        var test = testovi[i].split(",");
        zahtjev = { operacija: test[0], ruta: test[1], ulaz: test[2], izlaz: test[3] };

        if (zahtjev.operacija.toLowerCase() == "get") {

            indeksi.push(i);

            it("GET test", (done) => {

                var test = testovi[indeksi[j++]].split(",");
                zahtjev = { operacija: test[0], ruta: test[1], ulaz: test[2], izlaz: test[3] };

                if(test.length > 4){
                    for(let i = 4; i < test.length; i++){
                        zahtjev.izlaz = zahtjev.izlaz + "," + test[i];
                    }
                }

                chai.request(server)
                    .get(zahtjev.ruta)
                    .end(function (err, response) {

                        if (err) throw err;

                        response.should.have.status(200);
                        response.body.should.be.a("array");
                        expect(response.body.toString()).to.equal(JSON.parse(zahtjev.izlaz).toString());
                        
                        done();

                    });
            });



        } else if (zahtjev.operacija.toLowerCase() == "post") {


            indeksi.push(i);

            it("POST test", (done) => {

                var test = testovi[indeksi[j++]].split(",");

                zahtjev = { operacija: test[0], ruta: test[1], ulaz: test[2], izlaz: test[3] };

                if(zahtjev.ruta.toLowerCase() == "/aktivnost"){
                    for(let i = 3; i < test.length - 1; i++){
                        zahtjev.ulaz = zahtjev.ulaz + "," + test[i];
                    }
                }
                zahtjev.izlaz = test[test.length - 1];
                

                chai.request(server)
                    .post(zahtjev.ruta)
                    .send(JSON.parse(zahtjev.ulaz))
                    .end(function (err, response) {

                        if (err) throw err;

                        response.should.have.status(200);
                        response.body.should.have.property("message").eql(JSON.parse(zahtjev.izlaz)["message"]);
                        done();


                    });
            });




        } else if (zahtjev.operacija.toLowerCase() == "delete") {

            indeksi.push(i);

            it("DELETE test", (done) => {

                var test = testovi[indeksi[j++]].split(",");
                zahtjev = { operacija: test[0], ruta: test[1], ulaz: test[2], izlaz: test[3] };

                chai.request(server)
                    .del(zahtjev.ruta)
                    .end(function (err, response) {

                        if (err) throw err;

                        response.should.have.status(200);
                        response.body.should.have.property("message").eql(JSON.parse(zahtjev.izlaz)["message"]);
                        done();

                    });
            });


        } else {
            console.log("Greška - nepostojeća metoda");
        }

    }

});









