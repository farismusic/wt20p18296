let assert = chai.assert;
describe('Raspored', function () {
    describe('iscrtajRaspored()', function () {

        it('Treba iscrtati raspored sa 6 redova (5 dana i jedan red za sate)', function () {

            let ras = document.getElementById("raspored");

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak"], 8, 21);

            let redovi = ras.getElementsByTagName("table")[0].getElementsByTagName("tr");
            
            assert.equal(redovi.length, 6, "Broj redova treba biti 6");
        });

        it('Treba iscrtati raspored sa 3 reda (2 dana i jedan red za sate)', function () {

            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak"], 10, 20);

            let redovi = ras.getElementsByTagName("table")[0].getElementsByTagName("tr");
            
            assert.equal(redovi.length, 3, "Broj redova treba biti 3");
        });

        it('Niti jedan dan u sedmici', function () {

            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, [], 10, 20);

            let redovi = ras.getElementsByTagName("table")[0].getElementsByTagName("tr");
            
            assert.equal(redovi.length, 1, "Broj redova treba biti 1");
        });

        it('Potrebno kreirati 9 kolona, uključujući onu sa danima', function () {

            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak"], 8, 12);

            let kolone = ras.getElementsByTagName("table")[0].getElementsByTagName("tr")[1].getElementsByTagName("td");
            
            assert.equal(kolone.length, 9, "Broj kolona treba biti 9");
        });

        it('Nepostojeći div', function () {

            let ras = document.getElementById("nepostojeći");
            
            assert.equal(Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak"], 8, 12), 0, "Nepostojeći div");
        });

        it('Za sate 8 - 10 treba prikazati samo 8 jer se zadnji sat ne prikazuje', function () {
            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak"], 8, 10);

            let kolone = ras.getElementsByTagName("table")[0].getElementsByTagName("tr")[0].getElementsByTagName("td");

            let brojac = 0;

            for(let i = 0; i < kolone.length; i++){
                if(kolone[i].getElementsByTagName("p").length != 0){
                    brojac++;
                }
            }
        
            assert.equal(brojac, 1, "Jedan sat se prikazuje");
        });

        it('Za sate 0 - 10 treba prikazati samo 0, 2, 4, 6, 8', function () {
            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak"], 0, 10);

            let kolone = ras.getElementsByTagName("table")[0].getElementsByTagName("tr")[0].getElementsByTagName("td");

            let brojac = 0;

            for(let i = 0; i < kolone.length; i++){
                if(kolone[i].getElementsByTagName("p").length != 0){
                    brojac++;
                }
            }
        
            assert.equal(brojac, 5, "Treba prikazati 0, 2, 4, 6, 8");
        });

        it('Pocetak > kraj', function () {
            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak"], 10, 9);

            let tabele = ras.getElementsByTagName("table");
        
            assert.equal(tabele.length, 0, "Greška, tabela se neće kreirati jer sati nisu ispravni");
        });

        it('Negativne vrijednosti', function () {
            
            let ras = document.getElementById("raspored");

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak"], -7, 100);

            let tabele = ras.getElementsByTagName("table");
        
            assert.equal(tabele.length, 0, "Greška, tabela se neće kreirati jer sati nisu ispravni");
        });

        it('Pocetak = kraj', function () {
            
            let ras = document.getElementById("raspored");

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak"], 5, 5);

            let tabele = ras.getElementsByTagName("table");
        
            assert.equal(tabele.length, 0, "Greška, tabela se neće kreirati jer sati nisu ispravni");
        });

    });

    describe('dodajAktivnost', function () {

        it('Ispravno dodavanje aktivnosti', function () {

            let ras = document.getElementById("raspored");

            ras.innerHTML = '';

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak"], 8, 21);

            Raspored.dodajAktivnost(ras, "WT", "test1", 8, 11, "Ponedjeljak");

            let cell = ras.getElementsByTagName("table")[0].getElementsByTagName("tr")[1].getElementsByTagName("td")[1];
            
            assert.equal(cell.getElementsByTagName("p")[0].textContent, "WT test1", "U ćeliju će biti upisan WT test1");
        });

        it('Dodavanje 2 aktivnosti', function () {

            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak"], 10, 20);

            Raspored.dodajAktivnost(ras, "WT", "test2", 10, 11, "Ponedjeljak");
            Raspored.dodajAktivnost(ras, "WT", "neispravno",11, 12, "Ponedjeljak");

            let cells = ras.getElementsByTagName("table")[0].getElementsByTagName("tr")[1]. getElementsByTagName("td");

            let brojac = 0;
            for(let i = 1; i < cells.length; i++){
                if(cells[i].getElementsByTagName("p").length != 0){
                    brojac++;
                }
            }
            
            assert.equal(brojac, 2, "Treba biti 2 paragrafa u prvom redu");
        });

        it('Dodavanje 1 aktivnosti na cijeli dan', function () {

            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak"], 10, 20);

            Raspored.dodajAktivnost(ras, "WT", "test3", 10, 20, "Ponedjeljak");

            let cells = ras.getElementsByTagName("table")[0].getElementsByTagName("tr")[1]. getElementsByTagName("td");

            let brojac = 0;
            for(let i = 1; i < cells.length; i++){
                if(cells[i].getElementsByTagName("p").length != 0){
                    brojac++;
                }
            }
            
            assert.equal(brojac, 1, "Treba biti 1 paragraf preko cijelog reda");
        });

        it('Preklapanje', function () {

            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak"], 10, 20);

            Raspored.dodajAktivnost(ras, "WT", "test3", 10, 20, "Ponedjeljak");
            
            assert.equal(Raspored.dodajAktivnost(ras, "WT", "test3", 11, 19, "Ponedjeljak"), 2, "Preklapanje termina");
        });

        it('Nepostojeći dan', function () {

            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak"], 10, 20);

            Raspored.dodajAktivnost(ras, "WT", "test3", 10, 20, "Ponedjeljak");
            
            assert.equal(Raspored.dodajAktivnost(ras, "WT", "test3", 11, 19, "Petak"), 1, "Dan ne postoji u rasporedu");
        });

        it('Nepostojeće vrijeme', function () {

            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak"], 10, 20);   
            Raspored.dodajAktivnost(ras, "WT", "test3", 10, 20, "Ponedjeljak");
            
            assert.equal(Raspored.dodajAktivnost(ras, "WT", "test3", 11, 21, "Petak"), 1, "Vrijeme ne postoji u rasporedu");
        });

        it('Sve okej', function () {

            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak"], 10, 20);

            Raspored.dodajAktivnost(ras, "WT", "test3", 10, 20, "Ponedjeljak");
            
            assert.equal(Raspored.dodajAktivnost(ras, "WT", "test6", 10, 11, "Srijeda"), null, "Dodavanje aktivnosti je uspješno");
        });

        it('Sve okej uspješne', function () {

            
            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak"], 10, 20);
            
            Raspored.provjeriAlert(Raspored.dodajAktivnost(ras, "WT", "test3", 10, 11, "Ponedjeljak"));
            Raspored.provjeriAlert(Raspored.dodajAktivnost(ras, "WT", "test6", 11.5, 12, "Srijeda"));
            Raspored.provjeriAlert(Raspored.dodajAktivnost(ras, "WT", "test6", 11.5, 12, "Utorak"));
            Raspored.provjeriAlert(Raspored.dodajAktivnost(ras, "WT", "test6", 11.5, 12, "Četvrtak"));
            
            assert.equal(Raspored.provjeriAlert(Raspored.dodajAktivnost(ras, "WT", "test3", 11, 12, "Ponedjeljak")), null, "Nijedan alert nije izbačen");
        });

        it('Negativan sat', function () {

            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak"], 10, 20);
            
            Raspored.provjeriAlert(Raspored.dodajAktivnost(ras, "WT", "test3", 10, 11, "Ponedjeljak"));
            
            assert.equal(Raspored.dodajAktivnost(ras, "WT", "test3", -11, 12, "Ponedjeljak"), 1, "Greška - negativni sati");
        });

        it('Dvije aktivnosti počinju u isto vrijeme', function () {

            let ras = document.getElementById("raspored");

            ras.getElementsByTagName("table")[0].remove();

            Raspored.iscrtajRaspored(ras, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak"], 10, 20);
            
            Raspored.provjeriAlert(Raspored.dodajAktivnost(ras, "WT", "test3", 10, 11, "Ponedjeljak"));
            
            assert.equal(Raspored.dodajAktivnost(ras, "WT", "test3", 10, 12, "Ponedjeljak"), 2, "Greška - negativni sati");
        });

    });
});
