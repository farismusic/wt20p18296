var grupeAjax = new XMLHttpRequest();

var grupe = [];

function dohvatiGrupe() {

    grupeAjax.onreadystatechange = function () {
        if (grupeAjax.readyState == 4 && grupeAjax.status == 200) {

            grupe = JSON.parse(grupeAjax.response);

            let lista = document.getElementById("grupe");

            grupe.forEach(element => {
                let opt = document.createElement('option');
                opt.appendChild(document.createTextNode(element["naziv"]));
                opt.value = element["id"];
                lista.appendChild(opt);
            });

        }

        if (grupeAjax.readyState == 4 && grupeAjax.status == 404) {

            console.log("Pogrešan URL");

        }

    }
    grupeAjax.open("GET", "http://localhost:3000/v2/grupe", true);
    grupeAjax.send();

}

function dodajStudente() {

    let lista = document.getElementById("grupe");
    let textArea = document.getElementById("studenti");

    if(lista.options[lista.selectedIndex] == undefined || textArea.value == ""){
        return;
    }

    grupa = { id: lista.value, naziv: lista.options[lista.selectedIndex].text };


    s = textArea.value.trim().split("\n");

    let studenti = [];

    s.forEach(el => {
        studenti.push({ ime: el.split(",")[0].trim(), indeks: el.split(",")[1].trim() });
    });

    studenti.push(grupa);

    let dodajStudenteAjax = new XMLHttpRequest();

    dodajStudenteAjax.onreadystatechange = function () {
        if (dodajStudenteAjax.readyState == 4 && dodajStudenteAjax.status == 200) {

            odgovori = JSON.parse(dodajStudenteAjax.response);
            
            let string = "";
            odgovori.forEach(o => {
                string += "- " + o["message"] + "\n";
            })

            textArea.value = string
            
        }

        if (dodajStudenteAjax.readyState == 4 && dodajStudenteAjax.status == 404) {

            console.log("Pogrešan URL");

        }

    }

    dodajStudenteAjax.open("POST", "http://localhost:3000/v2/addStudents", true);
    dodajStudenteAjax.setRequestHeader("Content-Type", "application/json");
    dodajStudenteAjax.send(JSON.stringify(studenti));

}


