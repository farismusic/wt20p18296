let Raspored = (function () {

    var pocetniSat, krajnjiSat, daniRasporeda; 

    function iscrtajRaspored(div, dani, satPocetak, satKraj) {

        if (div == null || div == undefined) {
            return 0;
        }
    
        //Globalne varijable
        pocetniSat = satPocetak;
        krajnjiSat = satKraj;
        daniRasporeda = dani;
        //-------------------
    
        var brojDana = dani.length;
        var pocetak = satPocetak;
    
        var sati = new Map([
            [0, "00:00"],
            [2, "02:00"],
            [4, "04:00"],
            [6, "06:00"],
            [8, "08:00"],
            [10, "10:00"],
            [12, "12:00"],
            [15, "15:00"],
            [17, "17:00"],
            [19, "19:00"],
            [21, "21:00"],
            [23, "23:00"]
        ]);
    
        if (satPocetak >= satKraj || (satPocetak < 0 || satPocetak > 24) || (satKraj < 0 || satKraj > 24)) {
            div.innerHTML = "Greška";
            return;
        }
    
        var tabela = document.createElement("table");
        var s = 0;
    
    
        for (let i = 0; i < brojDana + 1; i++) {
    
            var red = document.createElement("tr");
    
            for (let j = 0; j < ((satKraj - satPocetak) * 2) + 1; j++) {
    
                var polje = document.createElement("td");
    
                if (i == 0) {
                    red.setAttribute("class", "sati");
                    polje.style.border = "none";
                    polje.style.backgroundColor = "white";
    
                    if (j % 2 != 0) {
    
                        if (sati.get(pocetak) != undefined) {
                            var paragraf = document.createElement("p");
                            paragraf.setAttribute("id", "sat");
                            paragraf.innerHTML = sati.get(pocetak++);
                            polje.style.verticalAlign = "bottom";
                            polje.appendChild(paragraf);
                        } else {
                            pocetak++;
                        }
    
                    }
    
                } else if (j == 0) {
                    polje.innerHTML = dani[i - 1];
                    polje.setAttribute("class", "dan");
                } else {
                    polje.setAttribute("class", "noShow")
                }
                red.appendChild(polje);
            }
    
            tabela.appendChild(red);
        }
    
        div.appendChild(tabela);
    
        for (let i = 1; i < brojDana + 1; i++) {
            urediLinije(tabela.getElementsByTagName("tr")[i].getElementsByTagName("td"));
        }
    }
    
    function dodajAktivnost(raspored, naziv, tip, vrijemePocetak, vrijemeKraj, dan) {
    
        if (raspored == undefined || raspored == null || raspored.getElementsByTagName("table").length == 0) {
            alert("Greška - raspored nije kreiran");
            return;
        }
    
        var regex = /^[0-9]{1,2}(\.5)?$/;
    
        if (vrijemePocetak.toString().search(regex) == -1 || vrijemeKraj.toString().search(regex) == -1) {
            return 1;
        }
    
        if (vrijemePocetak >= vrijemeKraj || (vrijemePocetak < 0 || vrijemePocetak > 24) || (vrijemeKraj < 0 || vrijemeKraj > 24)) {
            return 1;
        }
    
        if (!(vrijemePocetak >= pocetniSat && vrijemePocetak < krajnjiSat) || !(vrijemeKraj > pocetniSat && vrijemeKraj <= krajnjiSat)) {
            return 1;
        }
    
        if (!daniRasporeda.includes(dan)) {
            return 1;
        }
    
        var redniDan = daniRasporeda.indexOf(dan) + 1;
        var slots = (vrijemeKraj - vrijemePocetak) * 2;
        var pos;
    
        var red = raspored.getElementsByTagName("table")[0].getElementsByTagName("tr")[redniDan];
    
    
        var cells = red.getElementsByTagName("td");
        var s = pocetniSat - 0.5;
        for (let i = 0; i < cells.length; i++) {
    
    
            if (cells[i].getAttribute("colspan") == null) {
                s += 0.5;
            } else {
                s += cells[i].getAttribute("colspan") / 2;
            }
    
            if (vrijemePocetak < s) {
                return 2;
    
            } else if (vrijemePocetak.toString() == s.toString()) {
    
    
                for (let z = i + 1; z < i + slots + 1; z++) {
                    if (cells[z].getElementsByTagName("p").length != 0) {
                        return 2;
                    }
                }
                pos = i + 1;
    
    
                for (let j = 0; j < slots - 1; j++) {
    
                    cells[pos + 1].remove();
                }
    
                var p = document.createElement("p");
                var node = document.createTextNode(naziv + " " + tip);
                p.appendChild(node);
    
                cells[pos].setAttribute("id", "fax")
                cells[pos].classList.remove("noShow");
                cells[pos].appendChild(p);
                cells[pos].setAttribute("colspan", slots);
                urediLinije(cells);
                return;
            }
    
        }
    }
    
    function urediLinije(red) {
    
        var obrni = 0;
    
        for (let i = 1; i < red.length; i++) {
    
            if (i % 2 == obrni) {
    
                if (red[i].getAttribute("colspan") == null) {
    
                    staviNaSolid(red[i]);
    
                } else if (red[i].getAttribute("colspan") % 2 == 0) {
    
                    if (obrni == 0) {
                        obrni = 1;
                    } else {
                        obrni = 0;
                    }
    
                } else {
    
                    staviNaSolid(red[i]);
    
    
    
                }
    
            } else {
    
                if (red[i].getAttribute("colspan") == null) {
    
                } else if (red[i].getAttribute("colspan") % 2 == 0) {
    
                    staviNaSolid(red[i]);
    
                    if (obrni == 0) {
                        obrni = 1;
                    } else {
                        obrni = 0;
                    }
    
    
                } else {
    
                }
    
            }
        }
    
    
    }
    
    function staviNaSolid(cell) {
        cell.style.borderRight = "1px solid black";
    }
    
    function provjeriAlert(kod) {
        if (kod == null) {
            return;
        } else if (kod == 0) {
            alert("Div ne postoji");
        } else if (kod == 1) {
            alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
        } else if (kod == 2) {
            alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
        }
    }
    
    return{
        iscrtajRaspored: iscrtajRaspored,
        dodajAktivnost: dodajAktivnost,
        provjeriAlert: provjeriAlert
    }
}());