var dohvatiPred = new XMLHttpRequest();
var dohvatiGr = new XMLHttpRequest();
var dohvatiTip = new XMLHttpRequest();
var dohvatiDan = new XMLHttpRequest();

var predmeti = [];
var tipovi = [];
var dani = [];
var grupe = [];

function dohvatiPredmete() {

    dohvatiPred.onreadystatechange = function () {
        if (dohvatiPred.readyState == 4 && dohvatiPred.status == 200) {

            predmeti = JSON.parse(dohvatiPred.response);

            let lista = document.getElementById("predmet");

            predmeti.forEach(element => {
                let opt = document.createElement('option');
                opt.appendChild(document.createTextNode(element["naziv"]));
                opt.value = element["id"];
                lista.appendChild(opt);
            });

            dohvatiGr.onreadystatechange = function () {
                if (dohvatiGr.readyState == 4 && dohvatiGr.status == 200) {

                    grupe = JSON.parse(dohvatiGr.response);

                    let lista = document.getElementById("grupa");

                    grupe.forEach(element => {
                        let opt = document.createElement('option');
                        opt.appendChild(document.createTextNode(element["naziv"]));
                        opt.value = element["id"];
                        lista.appendChild(opt);
                    });

                }

                if (dohvatiGr.readyState == 4 && dohvatiGr.status == 404) {

                    console.log("Pogrešan URL");

                }

            }

            dohvatiGr.open("GET", "http://localhost:3000/v2/grupePredmeta/" + document.getElementById("predmet").value, true);
            dohvatiGr.send();

        }

        if (dohvatiPred.readyState == 4 && dohvatiPred.status == 404) {

            console.log("Pogrešan URL");

        }

    }

    dohvatiPred.open("GET", "http://localhost:3000/v2/predmeti", true);
    dohvatiPred.send();

}

function dohvatiTipove() {

    dohvatiTip.onreadystatechange = function () {
        if (dohvatiTip.readyState == 4 && dohvatiTip.status == 200) {

            tipovi = JSON.parse(dohvatiTip.response);

            let lista = document.getElementById("tip");

            tipovi.forEach(element => {
                let opt = document.createElement('option');
                opt.appendChild(document.createTextNode(element["naziv"]));
                opt.value = element["id"];
                lista.appendChild(opt);
            });

        }

        if (dohvatiTip.readyState == 4 && dohvatiTip.status == 404) {

            console.log("Pogrešan URL");

        }

    }

    dohvatiTip.open("GET", "http://localhost:3000/v2/tipovi", true);
    dohvatiTip.send();

}

function dohvatiDane() {

    dohvatiDan.onreadystatechange = function () {
        if (dohvatiDan.readyState == 4 && dohvatiDan.status == 200) {

            dani = JSON.parse(dohvatiDan.response);

            let lista = document.getElementById("dan");

            dani.forEach(element => {
                let opt = document.createElement('option');
                opt.appendChild(document.createTextNode(element["naziv"]));
                opt.value = element["id"];
                lista.appendChild(opt);
            });

        }

        if (dohvatiDan.readyState == 4 && dohvatiDan.status == 404) {

            console.log("Pogrešan URL");

        }

    }

    dohvatiDan.open("GET", "http://localhost:3000/v2/dani", true);
    dohvatiDan.send();

}

function dohvati() {

    dohvatiPredmete();
    dohvatiTipove();
    dohvatiDane();

}

document.getElementById("predmet").addEventListener("change", () => {

    var lista = document.getElementById("grupa");

    while (lista.firstChild) {
        lista.removeChild(lista.lastChild);
    }

    let grupePredmeta = new XMLHttpRequest();

    grupePredmeta.onreadystatechange = function () {// Anonimna funkcija
        if (grupePredmeta.readyState == 4 && grupePredmeta.status == 200) {

            var odgovor = JSON.parse(grupePredmeta.response);

            odgovor.forEach(o => {

                let opt = document.createElement('option');
                opt.appendChild(document.createTextNode(o["naziv"]));
                opt.value = o["id"];
                lista.appendChild(opt);

            });

        }

        if (grupePredmeta.readyState == 4 && grupePredmeta.status == 404) {
            console.log("Pogrešan url");
        }

    }

    grupePredmeta.open("GET", "http://localhost:3000/v2/grupePredmeta/" + document.getElementById("predmet").value, true);
    grupePredmeta.send();

})


function dodajAktivnost() {


    //var naziv = document.getElementById("naziv").value.trim();
    var pocetak = document.getElementById("pocetak");
    var kraj = document.getElementById("kraj");
    var tip = document.getElementById("tip");
    var dan = document.getElementById("dan");
    var predmet = document.getElementById("predmet");
    var grupa = document.getElementById("grupa");


    if (predmet.options[predmet.selectedIndex] == undefined || tip.options[tip.selectedIndex] == undefined || dan.options[dan.selectedIndex] == undefined || grupa.options[grupa.selectedIndex] == undefined) {
        document.getElementById("novaAktivnost").innerHTML = "Nepotpuni podaci!";
        return;
    }


    var ajax = new XMLHttpRequest();
    var novaAktivnost = { naziv: predmet.options[predmet.selectedIndex].text + " " + tip.options[tip.selectedIndex].text, pocetak: pocetak.value, kraj: kraj.value, predmetId: predmet.value, danId: dan.value, tipId: tip.value, grupaId: grupa.value };


    ajax.onreadystatechange = function () {// Anonimna funkcija
        if (ajax.readyState == 4 && ajax.status == 200) {

            var odgovor = JSON.parse(ajax.response)["message"];
            document.getElementById("novaAktivnost").innerHTML = odgovor;

        }

        if (ajax.readyState == 4 && ajax.status == 404) {
            console.log("Pogrešan url");
        }

    }

    ajax.open("POST", "http://localhost:3000/v2/aktivnost", true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(JSON.stringify(novaAktivnost));

}

