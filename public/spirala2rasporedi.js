okvir1 = document.getElementById("raspored1");


provjeriAlert(iscrtajRaspored(okvir1, ["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota", "Nedjelja"], 8, 20));

var dohvatiAkt = new XMLHttpRequest();

var aktivnosti = [];


function dohvatiAktivnosti() {

    dohvatiAkt.onreadystatechange = function () {// Anonimna funkcija
        if (dohvatiAkt.readyState == 4 && dohvatiAkt.status == 200) {

            aktivnosti = JSON.parse(dohvatiAkt.response);

            //console.log("Aktivnosti uspješno dohvaćene");

            for (let i = 0; i < aktivnosti.length; i++) {
                akt = {naziv:aktivnosti[i]["naziv"], tip:aktivnosti[i]["tip"], pocetak:aktivnosti[i]["pocetak"], kraj:aktivnosti[i]["kraj"], dan:aktivnosti[i]["dan"]};
                provjeriAlert(dodajAktivnost(okvir1, akt.naziv, akt.tip, parseFloat(akt.pocetak), parseFloat(akt.kraj), akt.dan));
            }
        }

        if (dohvatiAkt.readyState == 4 && dohvatiAkt.status == 404) {
            //console.log("Pogrešan URL");
        }

    }

    dohvatiAkt.open("GET", "http://localhost:3000/aktivnosti", true);
    dohvatiAkt.send();

}


/*Predavanja
provjeriAlert(dodajAktivnost(okvir1,"PWS", "predavanje",15,17,"Ponedjeljak"));
provjeriAlert(dodajAktivnost(okvir1,"OOI","predavanje",12,15,"Utorak"));
provjeriAlert(dodajAktivnost(okvir1,"OIS","predavanje",15,17,"Utorak"));
provjeriAlert(dodajAktivnost(okvir1,"WT","predavanje",9,12,"Srijeda"));
provjeriAlert(dodajAktivnost(okvir1,"RG","predavanje",9,12,"Četvrtak"));
provjeriAlert(dodajAktivnost(okvir1,"PJP","predavanje",9,12,"Petak"));

//Vježbe
provjeriAlert(dodajAktivnost(okvir1,"RG","vježbe",9,11,"Ponedjeljak"));
provjeriAlert(dodajAktivnost(okvir1,"PJP","vježbe",11,12,"Ponedjeljak"));
provjeriAlert(dodajAktivnost(okvir1,"OOI","vježbe",12,13,"Ponedjeljak"));
provjeriAlert(dodajAktivnost(okvir1,"RG","tutorijal",12,13,"Četvrtak"));
provjeriAlert(dodajAktivnost(okvir1,"WT","vježbe",15.5,17,"Petak"));
*/