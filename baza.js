const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2018296", "root", "root", { host: "127.0.0.1", dialect: "mysql", logging: false });
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//import modela
db.aktivnost = sequelize.import(__dirname + '/modeli/aktivnost.js');
db.dan = sequelize.import(__dirname + '/modeli/dan.js');
db.grupa = sequelize.import(__dirname + '/modeli/grupa.js');
db.predmet = sequelize.import(__dirname + '/modeli/predmet.js');
db.student = sequelize.import(__dirname + '/modeli/student.js');
db.tip = sequelize.import(__dirname + '/modeli/tip.js');

//relacije
// Veze 1-n 
db.predmet.hasMany(db.grupa, { foreignKey: { allowNull: false }, as: 'grupePredmeta' });
db.predmet.hasMany(db.aktivnost, { foreignKey: { allowNull: false }, as: 'aktivnostiPredmeta' });
db.dan.hasMany(db.aktivnost, { foreignKey: { allowNull: false }, as: 'dnevneAktivnosti' });
db.tip.hasMany(db.aktivnost, { foreignKey: { allowNull: false }, as: 'aktivnostiTipa' });

// veza 0-n. U tabeli aktivnost, kolona grupa_id može biti null. PROVJERITI !! Jedna aktivnost ima samo jednu grupu.
db.grupa.hasMany(db.aktivnost, { as: 'aktivnostiTipa' });

// Veza n-m 
db.studentGrupa = db.student.belongsToMany(db.grupa, { as: 'grupe', through: 'student_grupa', foreignKey: 'studentId' });
db.grupa.belongsToMany(db.student, { as: 'studenti', through: 'student_grupa', foreignKey: 'grupaId' });


module.exports = db;