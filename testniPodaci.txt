DELETE,/v1/all,null,{"message":"Uspješno obrisan sadržaj datoteka!"}
GET,/v1/predmeti,null,[]
POST,/v1/predmet,{"naziv":"RMA"},{"message":"Uspješno dodan predmet!"}
POST,/v1/predmet,{"naziv":"WT"},{"message":"Uspješno dodan predmet!"}
GET,/v1/predmeti,null,[{"naziv":"RMA"},{"naziv":"WT"}]
POST,/v1/predmet,{"naziv":"OOI"},{"message":"Uspješno dodan predmet!"}
GET,/v1/predmeti,null,[{"naziv":"RMA"},{"naziv":"WT"},{"naziv":"OOI"}]
DELETE,/v1/all,null,{"message":"Uspješno obrisan sadržaj datoteka!"}
GET,/v1/aktivnosti,null,[]
POST,/v1/aktivnost,{"naziv" : "OOI","tip" : "Vježbe","pocetak" : 9,"kraj" : 12,"dan" : "Četvrtak"},{"message":"Uspješno dodana aktivnost!"}
POST,/v1/aktivnost,{"naziv" : "RG","tip" : "Predavanje","pocetak" : 8,"kraj" : 9,"dan" : "Petak"},{"message":"Uspješno dodana aktivnost!"}
POST,/v1/aktivnost,{"naziv" : "PWS","tip" : "Tutorijal","pocetak" : 10,"kraj" : 11,"dan" : "Četvrtak"},{"message":"Aktivnost nije validna!"}
GET,/v1/aktivnosti,null,[{"naziv" : "OOI","tip" : "Vježbe","pocetak" : 9,"kraj" : 12,"dan" : "Četvrtak"},{"naziv" : "RG","tip" : "Predavanje","pocetak" : 8,"kraj" : 9,"dan" : "Petak"}]
GET,/v1/predmet/OOI/aktivnost,null,[{"naziv" : "OOI","tip" : "Vježbe","pocetak" : 9,"kraj" : 12,"dan" : "Četvrtak"}]
POST,/v1/aktivnost,{"naziv" : "OOI","tip" : "Predavanje","pocetak" : 8,"kraj" : 9,"dan" : "Ponedjeljak"},{"message":"Uspješno dodana aktivnost!"}
GET,/v1/predmet/OOI/aktivnost,null,[{"naziv" : "OOI","tip" : "Vježbe","pocetak" : 9,"kraj" : 12,"dan" : "Četvrtak"},{"naziv" : "OOI","tip" : "Predavanje","pocetak" : 8,"kraj" : 9,"dan" : "Ponedjeljak"}]
DELETE,/v1/aktivnost/OOI,null,{"message":"Uspješno obrisana aktivnost!"}
DELETE,/v1/aktivnost/RG,null,{"message":"Uspješno obrisana aktivnost!"}
GET,/v1/aktivnosti,null,[]
POST,/v1/predmet,{"naziv":"RMA"},{"message":"Uspješno dodan predmet!"}
POST,/v1/predmet,{"naziv":"WT"},{"message":"Uspješno dodan predmet!"}
POST,/v1/predmet,{"naziv":"WT"},{"message":"Naziv predmeta postoji!"}
DELETE,/v1/predmet/RMA,null,{"message":"Uspješno obrisan predmet!"}
DELETE,/v1/all,null,{"message":"Uspješno obrisan sadržaj datoteka!"}
POST,/v1/predmet,{"naziv":"AFJ"},{"message":"Uspješno dodan predmet!"}
GET,/v1/predmeti,null,[{"naziv":"AFJ"}]
POST,/v1/predmet,{"naziv":"LD"},{"message":"Uspješno dodan predmet!"}
GET,/v1/predmeti,null,[{"naziv":"AFJ"},{"naziv":"LD"}]
DELETE,/v1/predmet/PJP,null,{"message":"Greška - predmet nije obrisan!"}
DELETE,/v1/predmet/LD,null,{"message":"Uspješno obrisan predmet!"}
POST,/v1/aktivnost,{"naziv":"AFJ","tip":"predavanje","pocetak":"9","kraj":"12","dan":"ponedjeljak"},{"message":"Uspješno dodana aktivnost!"}
GET,/v1/aktivnosti,null,[{"naziv":"AFJ","tip":"predavanje","pocetak":"9","kraj":"12","dan":"ponedjeljak"}]
POST,/v1/aktivnost,{"naziv":"LD","tip":"predavanje","pocetak":"11","kraj":"14","dan":"ponedjeljak"},{"message":"Aktivnost nije validna!"}
DELETE,/v1/aktivnost/AFJ,null,{"message":"Uspješno obrisana aktivnost!"}
POST,/v1/aktivnost,{"naziv":"LD","tip":"predavanje","pocetak":"11","kraj":"14","dan":"ponedjeljak"},{"message":"Uspješno dodana aktivnost!"}
GET,/v1/aktivnosti,null,[{"naziv":"LD","tip":"predavanje","pocetak":"11","kraj":"14","dan":"ponedjeljak"}]
POST,/v1/aktivnost,{"naziv":"LD","tip":"tutorijal","pocetak":"11","kraj":"14","dan":"utorak"},{"message":"Uspješno dodana aktivnost!"}
DELETE,/v1/aktivnost/LD,null,{"message":"Uspješno obrisana aktivnost!"}
GET,/v1/predmet/LD/aktivnost,null,[]
DELETE,/v1/all,null,{"message":"Uspješno obrisan sadržaj datoteka!"}